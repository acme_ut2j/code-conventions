# Code conventions

## Definitions

**`else if` statement:** an `else` statement, whose content is an `if` statement
followed by any number of `else if` statements and an optional `else` statement

## Global

- Use 4 space indents (no tabs in the source code)
- Lines should be no wider than 88 columns
- Make non-fast forwarding squash merges
- Automate testing with Selenium
- No line breaks before the opening brace, one line break before the closing brace,
  use the same indent for the closing brace as the line with the matching opening brace
  - ✅This is okay:
  ```javascript
  if (stuff) {
      foo();
  } else if (qux()) {
      bar();
  }
  for (let theThing of things)
      if (theThing.isWeird()) {
          frobnicate(theThing);
      }
  ```
  - ❎This is not:
  ```javascript
  if (stuff)
  {
      foo();
  } else if (qux()) {
      bar();
      }
  for (let theThing of things)
  {   if (theThing.isWeird())
          {frobnicate(theThing);}
  }
  ```
- Never omit braces in a `if`, `else`, `while` or `for` statement unless their content
  is an `if` statement
- Use `camelCase` instead of `snake_case`
- COMMENT THE CODE!!!
- No more than one statement per line; in Python, this rule implies that the only places
  where semicolons are allowed are in comments, docstrings and string literals.
  - However, put the `else` and the `if` parts of an *`else if` statement* within the 
    same line. 
    - ✅This is okay:
      ```javascript
      if (toto) {
          return;
      } else if (titi) {
          tutu();
      }
      ```
    - ❎This is not:
      ```javascript
      if (toto) {
          return;
      } else
          if (titi) {
              tutu();
          }
      ```
- Files which do not contain code (either in source code form or machine code form)
  should be named using `snake_case`
- Use [guard statements](https://en.wikipedia.org/wiki/Guard_(computer_science))
  to reduce nesting and make the flow of the code more linear
  - ✅This is okay:
    ```javascript
    if (!toto()) {
        return titi();
    }
    if (!tata()) {
        return null;
    }
    if (!bar()) {
        return quux();
    }
    return foo;
    ```
  - ❎This is an example of the [arrow anti-pattern
    ](https://blog.codinghorror.com/flattening-arrow-code/) and is not okay:
    ```javascript
    if (toto()) {
        if (tata()) {
            if (bar()) {
                return foo;
            } else {
                return quux();
            }
        } else {
            return null;
        }
    } else {
        return titi();
    }
    ```
  - Note: in this example, it may not be clear why the latter style is less readable.
    However, in more complex code, the guard pattern helps much more. To better use the
    guard pattern, factor your code into a function.

### for-loop
- Prefer foreach loops (i.e. `for x in y` or `for (it of val)`) over C-style loops
  (i.e. `for (i = 0; i < lim; ++i)`); they are less prone to 
  [off-by-one errors](https://fr.wikipedia.org/wiki/Erreur_off-by-one).
- If the elements of the iterated-over value are indices or counters, use the 
  traditional `ii`,`jj`, `kk` variable names.
  - The `i`s, `j`s and `k`s are repeated for easier `grep`ability. Don't overuse them.
- If the iterated-over variable is a collection of items, and the variable's name is
  plural, the singular form of that name (if it isn't the same as the plural from) is a
  good candidate for naming the variable of the for-loop.
  - .e.g: `for row in rows`, `for student in students`, `for child in children`, etc.

## JavaScript & PHP

- Use the `===` operator instead of the `==` operator
- Use `let` instead of `var`
- Use ? to automatically format the code
- *Cuddle the `else`s:* put the `else` statement in the same line as its preceding
  brace, if there is any.
  - ✅ i.e. do this:
  ```javascript
  if (toto) {
      return titi();
  } else {
      return tata();
  }
  ```
  - ❎ instead of this:
  ```javascript
  if (toto) {
      return titi();
  }
  else {
      return tata();
  }
  ```javascript

## Python

- Use `black` to automatically format the code
- Use `flake8` to check the code
- Whenever a rule conflicts with [PEP 8](https://www.python.org/dev/peps/pep-0008/),
  follow PEP 8 instead.
  - **N.B.:** the 88 column limit might on first glance be considered a violation of
    PEP 8, but PEP 8 allows for a team to choose a greater line limit:
    > Some teams strongly prefer a longer line length. For code maintained exclusively
    > or primarily by a team that can reach agreement on this issue, it is okay to
    > increase the line length limit up to 99 characters, provided that comments and
    > docstrings are still wrapped at 72 characters.

## SQL

- Names of SQL tables should reflect the type of the entries of the table, 
  **pluralized**.
  - e.g. a table of dice should be called `` `dés` ``, a table of bananas should be
    called `` `bananes` ``.
- Identifiers (i.e. table or field names) should use `snake_case`.

## Commit messages

- TBD

## Branch naming

- TBD

